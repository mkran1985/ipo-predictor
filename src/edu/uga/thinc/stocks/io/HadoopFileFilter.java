package edu.uga.thinc.stocks.io;

import java.io.File;
import java.io.FilenameFilter;

public class HadoopFileFilter implements FilenameFilter {
	private static final String HADOOP_OUTPUT_PATTERN = "part-r-\\d+";
	
	public HadoopFileFilter() {
		
	}
	
	@Override
	public boolean accept(File dir, String name) {
		System.out.println("Examining name: \""+name+"\"");
		return name.matches(HADOOP_OUTPUT_PATTERN);
	}

}
