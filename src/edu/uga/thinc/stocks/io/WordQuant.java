package edu.uga.thinc.stocks.io;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

public class WordQuant implements Writable {
	private static final String WQ_EXPR = "\"[a-zA-Z0-9']+\"\\(\\d+\\)";
	
	
	private String word = "";
	private int count = 0;
	
	/* Constructors */
	public WordQuant() {
		
	}
	public WordQuant(String pWord) {
		word = pWord;
		count = 1;
	}
	
	public WordQuant(String pWord, int pCount) {
		word = pWord;
		count = pCount;
	}
	
	
	public static WordQuant parseWordQuant(String wqStr) {
		int start, end;
		if (!wqStr.matches(WQ_EXPR)) {
			return null;
		} else {
			WordQuant wq = new WordQuant();
			start = 1;
			end = wqStr.indexOf('\"', start);
			
			wq.setWord(wqStr.substring(start, end));
			start = wqStr.indexOf('(', end)+1;
			end = wqStr.indexOf(')', start);
			
			wq.setCount(new Integer(wqStr.substring(start, end)));
			return wq;
		}
	}
	
	/* Accessor Methods */
	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}


	public String toString() {
		return "\""+word+"\"("+Integer.toString(count)+")";
	}

	/* Read and write methods used for serialization */
	@Override
	public void readFields(DataInput in) throws IOException {
		
		word = in.readUTF();
		count = in.readInt();
	}

	@Override
	public void write(DataOutput out) throws IOException {

		out.writeUTF(word);
		out.writeInt(count);
	}

}
