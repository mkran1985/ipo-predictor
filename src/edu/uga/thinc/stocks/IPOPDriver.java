package edu.uga.thinc.stocks;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import edu.uga.thinc.stocks.io.*;
import edu.uga.thinc.stocks.mapper.*;
import edu.uga.thinc.stocks.reducer.*;

public class IPOPDriver extends Configured implements Tool {
	private static final String DEFAULT_COUNT_SRC = "articles";
	private static final String DEFAULT_COUNT_DEST = "word_counts";
	private static final String DEFAULT_FILTER_WORDS_DEST = "filtered_words";
	private static final String DEFAULT_CONSTRUCT_VECTORS_DEST = "vectors";
	private static final String WHITE_LIST_KEY = "filtered_words";

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		try {
			ToolRunner.run(new Configuration(), new IPOPDriver(), args);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	@Deprecated
	public int run(String[] arg0) throws Exception {
		FileUtils.deleteDirectory(new File(getConf().get("output")));
		FileUtils.deleteDirectory(new File("dimensions"));

		Path ipoPath = new Path(getConf().get("IPOs"));
		Path stockPath = new Path(getConf().get("stocks"));
		Path output = new Path(getConf().get("output"));
		Path vectors = new Path(getConf().get("vectors"));
		int reducerCount = getConf().getInt("reducers", 10);
		int features = getConf().getInt("features", 5000);
		int bins = getConf().getInt("bins", 20);
		
		//Load IPOs into hashmap
		HashMap<String, String> IPOs = new HashMap<String, String>();
		System.out.print("Retrieving IPOs and their dates...");
		FileSystem fs = FileSystem.get(new Configuration());
		FileStatus[] status = fs.listStatus(ipoPath);
		for (int i = 0; i < status.length; ++i) {

			BufferedReader br = new BufferedReader(new InputStreamReader(
					fs.open(status[i].getPath())));
			String line;
			line = br.readLine();
			while (line != null) {
				IPOs.put(line.split("\t")[0], line.split("\t")[1]);
				line = br.readLine();
			}
		}
		System.out.println("Done.");

		/**
		 * Phase 2
		 * 
		 * 1. Bucket the set of stocks a la LSH 2. For each IPO, generate KLD
		 * between IPO and all stocks (not including other IPOs) 3. Fit various
		 * models for predicting IPOs performance 30 days after going public
		 */
		double totalSpan = 0.0;

		// Preparing to perform LSH - get the spans to select hyperdimensions
		System.out.print("Spans calculating...");
		HashMap<String, double[]> wordSpan = new HashMap<String, double[]>();
		HashMap<String, HashMap<String, Double>> wordList = new HashMap<String, HashMap<String, Double>>();
		FileSystem.get(new Configuration());
		status = fs.listStatus(vectors);
		for (int i = 0; i < status.length; ++i) {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					fs.open(status[i].getPath())));
			String line;
			line = br.readLine();
			while (line != null) {
				// Get the words and their values
				String stockName = line.split("\t")[0].trim();
				String[] wordlist = line.split("\t")[1].trim().split(" ");
				if(!wordList.containsKey(stockName))
					wordList.put(stockName, new HashMap<String, Double>());

				for (String words : wordlist) {
					Pattern wordTextP = Pattern.compile("\"(.*)\"");
					Pattern wordValP = Pattern.compile("\\((.*)\\)");
					Matcher getWord = wordTextP.matcher(words);
					
					String wordText="";
					double wordVal = 0.0;
					if(getWord.find())
						wordText = getWord.group(1);
					getWord = wordValP.matcher(words);
					if(getWord.find())
						wordVal = Double.parseDouble(getWord.group(1));
					
					//Add to word list
					wordList.get(stockName).put(wordText, wordVal);
					if(wordList.get(stockName).get(wordText)!=wordVal)
						System.out.println("Major error in update");
					
					//System.out.println(wordText+": "+wordVal);
					if (wordSpan.containsKey(wordText)) {
						double[] spanVals = wordSpan.get(wordText);
						if (wordVal < spanVals[1]) {
							totalSpan-=spanVals[0];
							spanVals[1] = wordVal;
							spanVals[0] = spanVals[2] - spanVals[1];
							totalSpan+=spanVals[0];
						}
						if (wordVal > spanVals[2]) {
							totalSpan-=spanVals[0];
							spanVals[2] = wordVal;
							spanVals[0] = spanVals[2] - spanVals[1];
							totalSpan+=spanVals[0];
						}
						wordSpan.put(wordText, spanVals);
					} else {
						double[] spanVals = { 0.0, wordVal, wordVal };
						wordSpan.put(wordText, spanVals);
					}
				}

				line = br.readLine();
			}
		}
		System.out.println("Done.");
		
		Random rand = new Random(System.currentTimeMillis());
		HashMap<String, Double> hyperplanes = new HashMap<String, Double>();
		HashMap<String, double[]> candidates = (HashMap<String, double[]>) wordSpan.clone();
		System.out.print("Selecting hyperdimensions and calculating threshold...");
		for(int i=0;i<features;++i)
		{
			double roulette = rand.nextDouble()*totalSpan;
			for(Entry<String, double[]> span : candidates.entrySet())
			{
				roulette-=span.getValue()[0];
				if(roulette<=0)
				{
					//Calculate the threshold for this word
					int[] binCounts = new int[bins];
					for(int j=0;j<bins;++j) binCounts[j]=0;
					//Check how many stocks have words in this threshold
					for(Entry<String, HashMap<String, Double>> tCheck : wordList.entrySet())
					{
						if(tCheck.getValue().containsKey(span.getKey()))
						{
							int nextBin = (int) Math.floor((tCheck.getValue().get(span.getKey())-span.getValue()[1])/(span.getValue()[0]/(double)bins));
							if(nextBin==20)
								--nextBin;
							//System.out.println(nextBin);
							binCounts[nextBin]+=1;
						}
						else
						{
							binCounts[0]+=1;
						}
					}
					//Select index of minimum bin
					int minVal=99999999;
					int minIndex=0;
					for(int j=1;j<bins;++j)
					{
						if(binCounts[j]<binCounts[minIndex])
						{
							minVal=binCounts[j];
							minIndex=j;
						}
					}
					
					hyperplanes.put(span.getKey(), span.getValue()[1] + minIndex*span.getValue()[0]/(double)bins);
					candidates.remove(span.getKey());
					totalSpan-=span.getValue()[0];
					
					//System.out.println(span.getKey()+": "+hyperplanes.get(span.getKey()));
					break;
				}
				//System.out.println(span.getKey()+": ["+span.getValue()[0]+", "+span.getValue()[1]+", "+span.getValue()[2]+"]");
			}
		}
		System.out.println("Done.");
		
		/* Got the hyperdimensions and thresholds, begin bucketing */
		//Get signatures for each stock
		System.out.print("Bucketing...");
		HashMap<String, Vector<String>> buckets = new HashMap<String, Vector<String>>();
		for(Entry<String, HashMap<String, Double>> st : wordList.entrySet())
		{
			String sig = "";
			for(Entry<String, Double> hyperplane : hyperplanes.entrySet())
			{
				if(st.getValue().containsKey(hyperplane.getKey()))
				{
					if(st.getValue().get(hyperplane.getKey())<=hyperplane.getValue())
						sig+="1";
					else
						sig+="0";
				}
				else
					sig+="1";
			}
			if(buckets.containsKey(sig))
				buckets.get(sig).add(st.getKey());
			else
			{
				Vector<String> bkt = new Vector();
				bkt.add(st.getKey());
				buckets.put(sig, bkt);
			}
		}
		System.out.println("Done.");
		
		//Buckets will definitely be too sparse, merge P=0.75*M
		System.out.print("Initial merge...");
		HashSet<String> toRemove=new HashSet<String>();
		for(String key : buckets.keySet())
		{
			if(toRemove.contains(key))
				continue;
			//Find any neighbor with P=0.75*M
			String nbrKey = key;
			for(String dst : buckets.keySet())
			{
				if(key.equals(dst) || toRemove.contains(dst))
					continue;
				int dist = sigDiff(key, dst);
				if(dist<0.25*(double)hyperplanes.size())
				{
					nbrKey = dst;
					break;
				}
			}
			if(!nbrKey.equals(key))
			{
				buckets.get(key).addAll(buckets.get(nbrKey));
				toRemove.add(nbrKey);
			}
		}
		//Clean up buckets
		for(String key : toRemove)
			buckets.remove(key);
		System.out.println("Done.");
		
		//Incremental merge
		while(hasSingleIPO(buckets, IPOs))
		{
			System.out.print("Merging buckets...");
			toRemove=new HashSet<String>();
			for(String key : buckets.keySet())
			{
				if(toRemove.contains(key))
					continue;
				//Find closest neighbor
				int nbrDist = key.length();
				String nbrKey = key;
				for(String dst : buckets.keySet())
				{
					if(key.equals(dst) || toRemove.contains(dst))
						continue;
					int dist = sigDiff(key, dst);
					if(dist<nbrDist)
					{
						nbrDist = dist;
						nbrKey = dst;
					}
				}
				if(!nbrKey.equals(key))
				{
					buckets.get(key).addAll(buckets.get(nbrKey));
					toRemove.add(nbrKey);
				}
			}
			//Clean up buckets
			for(String key : toRemove)
				buckets.remove(key);
			/*for(Entry<String, Vector<String>> src : buckets.entrySet()) {
				for(Entry<String, Vector<String>> dst : buckets.entrySet()) {
					if(sigDiff(src.getKey(),src.getKey())<((double)hyperplanes.size()*0.75))
					{
						Vector<String> newNbrs = dst.getValue();
						Vector<String> oldNbrs = src.getValue();
						for(String nbr : oldNbrs)
							newNbrs.add(nbr);
						dst.setValue(newNbrs);
						buckets.remove(src.getKey());
						break;
					}
				}
			}*/
			System.out.println("Done.");
		}
		
		PrintWriter newFile = new PrintWriter(new BufferedWriter(new FileWriter("buckets.txt",false)));
		for(Entry<String, Vector<String>> bucket : buckets.entrySet())
		{
			//System.out.print(bucket.getKey()+"\t");
			String printOut = bucket.getKey()+"\t";
			for(String bkt : bucket.getValue())
			{
				printOut+=bkt+",";
				System.out.print(bkt+" ");
			}
			//Remove trailing comma
			printOut = printOut.substring(0, printOut.length()-1);
			newFile.println(printOut);
			System.out.println();
		}
		newFile.close();
		System.out.println(buckets.size()+" buckets.");
		/*
		 * FileSystem.get(getConf()).delete(output);] Job collect = new
		 * Job(getConf(), "IPOP_phase1");
		 * collect.setJarByClass(IPOPDriver.class);
		 * collect.setOutputKeyClass(Text.class);
		 * collect.setOutputValueClass(IntWritable.class);
		 * 
		 * collect.setNumReduceTasks(reducerCount);
		 * 
		 * collect.setMapperClass(IPOPM.class);
		 * collect.setReducerClass(IPOPR.class);
		 * 
		 * collect.setInputFormatClass(TextInputFormat.class);
		 * collect.setOutputFormatClass(TextOutputFormat.class);
		 * 
		 * FileInputFormat.addInputPath(collect, trainData);
		 * FileOutputFormat.setOutputPath(collect, output);
		 * 
		 * collect.waitForCompletion(true);
		 */
		return 0;
	}
	
	public int sigDiff(String str1, String str2)
	{
		int diff = 0;
		for(int i=0;i<str1.length();++i)
			if(str1.charAt(i)!=str2.charAt(i))
				++diff;
		return diff;
	}

	public Boolean hasSingleIPO(HashMap<String, Vector<String>> buckets, HashMap<String, String> IPOs)
	{
		for(Entry<String, Vector<String>> bucket : buckets.entrySet())
		{
			Boolean hasStock = false;
			for(String stock : bucket.getValue())
				if(!IPOs.containsKey(stock))
					hasStock=true;
			if(!hasStock)
				return true;
		}
		return false;
	}
	
	private int buildWordCounts(String src, String dest) throws Exception {
		
		
		Job countJob = Job.getInstance(new Configuration());
		countJob.setJarByClass(IPOPDriver.class);
		
		/* Set up the inputs and mappers */
		countJob.setMapperClass(WordCountMap.class);
		countJob.setMapOutputKeyClass(Text.class);
		countJob.setMapOutputValueClass(WordQuant.class);
		
		FileInputFormat.addInputPath(countJob, new Path(src));
		countJob.setInputFormatClass(TextInputFormat.class);
		
		/* Set up the outputs and reducers */
		countJob.setReducerClass(WordCountReduce.class);
		countJob.setOutputKeyClass(Text.class);
		countJob.setOutputValueClass(Text.class);
		
		FileOutputFormat.setOutputPath(countJob, new Path(dest));
		
		
		return (countJob.waitForCompletion(true) ? 0 : 1);
	}
	
	private int filterWords(String src, String dest) throws Exception {
		Job filterJob = Job.getInstance(new Configuration());
		
		filterJob.setJarByClass(IPOPDriver.class);
		
		/* Set up mapper and input */
		filterJob.setMapperClass(FilterWordsMap.class);
		filterJob.setMapOutputKeyClass(Text.class);
		filterJob.setMapOutputValueClass(WordQuant.class);
		
		filterJob.setInputFormatClass(TextInputFormat.class);
		FileInputFormat.addInputPath(filterJob, new Path(src));
		
		/* Set up reducer and output */
		filterJob.setReducerClass(FilterWordsReduce.class);
		filterJob.setOutputKeyClass(LongWritable.class);
		filterJob.setOutputValueClass(Text.class);
		
		FileOutputFormat.setOutputPath(filterJob, new Path(dest));
		
		return (filterJob.waitForCompletion(true) ? 0 : 1);
		
	}
	
	// XXX change to make use indices rather than words?
	public int constructVectors(String countSrc, String acceptableSrc, String dest) throws Exception {
		Job constructJob = Job.getInstance(new Configuration());
		
		constructJob.setJarByClass(IPOPDriver.class);
		
		/* Make the white list of vocabulary words accessible from the mapper */
		constructJob.addCacheFile(new URI(acceptableSrc));
		constructJob.getConfiguration().set(getWhiteListKey(), new Path(acceptableSrc).getName());
		
		/* Set up mapper and input */
		constructJob.setMapperClass(MatrixConstructionMap.class);
		constructJob.setMapOutputKeyClass(Text.class);
		constructJob.setMapOutputValueClass(WordQuant.class);
		
		constructJob.setInputFormatClass(TextInputFormat.class);
		FileInputFormat.addInputPath(constructJob, new Path(countSrc));
		
		/* Set up reducer and output */
		constructJob.setReducerClass(WordCountReduce.class);
		constructJob.setOutputKeyClass(Text.class);
		constructJob.setOutputKeyClass(Text.class);
		
		FileOutputFormat.setOutputPath(constructJob, new Path(dest));
		
		return (constructJob.waitForCompletion(true) ? 0 : 1);
	}
	
	public static String getWhiteListKey() {
		return WHITE_LIST_KEY;
	}
}