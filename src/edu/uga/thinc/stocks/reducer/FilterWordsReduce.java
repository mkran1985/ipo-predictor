package edu.uga.thinc.stocks.reducer;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import edu.uga.thinc.stocks.io.WordQuant;

public class FilterWordsReduce extends Reducer<Text, WordQuant, LongWritable, Text> {
	private static final long MINIMUM_QUANT_SYMBOLS = 1;
	@Override
	protected void reduce(Text keyIn, Iterable<WordQuant> valsIn,
			Context context)
			throws IOException, InterruptedException {
		
		boolean isUniform = true;
		int uniformCount = 0;
		long quantSymbols = 0;
		
		
		for (WordQuant quant : valsIn) {
			
			if (quantSymbols == 0) {
				uniformCount = quant.getCount();
			} else if (quant.getCount() != uniformCount) {
				isUniform = false;
			}
			
			++quantSymbols;
		}
		
		if (!isUniform && (quantSymbols > MINIMUM_QUANT_SYMBOLS)) {
			context.write(new LongWritable(quantSymbols), keyIn);
		}
	}

}
