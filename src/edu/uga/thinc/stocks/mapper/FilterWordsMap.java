package edu.uga.thinc.stocks.mapper;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import edu.uga.thinc.stocks.io.WordQuant;

public class FilterWordsMap extends Mapper<LongWritable, Text, Text, WordQuant> {
	private static final String SYMBOL_SPLIT_EXPR = "\t";
	private static final String COUNT_SPLIT_EXPR = "\\s";
	@Override
	protected void map(LongWritable key, Text value,
			Context context)
			throws IOException, InterruptedException {
			/* Parse the words for each symbol and send the resulting <word, <symbol, count> > pairs to be reduced*/
			String[] primarySplit = value.toString().split(SYMBOL_SPLIT_EXPR);
			String[] countSplit = primarySplit[1].split(COUNT_SPLIT_EXPR);
			
			WordQuant crrntQuant;
			String word;
			
			for (String wqStr : countSplit) {
				crrntQuant = WordQuant.parseWordQuant(wqStr);
				if (crrntQuant != null) {
					word = crrntQuant.getWord();
					crrntQuant.setWord(primarySplit[0]);
					context.write(new Text(word), crrntQuant);
				}
			}
		
		}

}
