#!/bin/bash
stocks=./filteredStocks.txt
while read p; do
	ipo=$(echo "$p" | awk '{ print $1 }') 
	grep -v "$ipo" $stocks > $stocks.$$.tmp
	mv $stocks.$$.tmp $stocks
done <IPOs.txt

