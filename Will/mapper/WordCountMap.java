package edu.uga.cs.csci6900.finalProject.mapper;

import java.io.IOException;
import java.util.HashMap;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import edu.uga.cs.csci6900.finalProject.io.WordQuant;

public class WordCountMap extends Mapper<LongWritable, Text, Text, WordQuant> {
	private static final String SYMBOL_SPLIT_EXPR = "\t";
	private static final String WORD_SPLIT_EXPR ="[^a-z']";
	@Override
	protected void map(LongWritable key, Text value,
			org.apache.hadoop.mapreduce.Mapper.Context context)
			throws IOException, InterruptedException {
			HashMap<String, Integer> wordMap = new HashMap<String, Integer>();
			
			String[] primarySplit = value.toString().split(SYMBOL_SPLIT_EXPR);
			String[] contentSplit = primarySplit[1].toLowerCase().split(WORD_SPLIT_EXPR);
			
			for (String word : contentSplit) {
				if (!wordMap.containsKey(word)) {
					wordMap.put(word, 1);
				} else {
					Integer crrntCount = wordMap.get(word);
					wordMap.put(word, new Integer(crrntCount.intValue() + 1));
				}
			}
			
			Text symbol = new Text(primarySplit[0]);
			for (String uniqueWord : wordMap.keySet()) {
				context.write(symbol, new WordQuant(uniqueWord, wordMap.get(uniqueWord).intValue()));
			}
	}

	
}
