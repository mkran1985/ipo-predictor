package edu.uga.thinc.stocks;


import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import edu.uga.cs.csci6900.finalProject.io.PositionValue;
import edu.uga.cs.csci6900.finalProject.io.WordQuant;
import edu.uga.cs.csci6900.finalProject.mapper.FilterWordsMap;
import edu.uga.cs.csci6900.finalProject.mapper.MatrixConstructionMap;
import edu.uga.cs.csci6900.finalProject.mapper.WordCountMap;
import edu.uga.cs.csci6900.finalProject.reducer.*;

/* This class is the driver that processes raw text into a set of vectors */
public class TextProcessor extends Configured implements Tool {
	private static final String DEFAULT_COUNT_SRC = "articles";
	private static final String DEFAULT_COUNT_DEST = "word_counts";
	private static final String DEFAULT_FILTER_WORDS_DEST = "filtered_words";
	private static final String DEFAULT_CONSTRUCT_VECTORS_DEST = "vectors";
	
	private static final String WHITE_LIST_KEY = "filtered_words";
	@Override
	public int run(String[] arg0) throws Exception {
		
		/* Build the set of word counts for each symbol */
		buildWordCounts("simple_data.txt", DEFAULT_COUNT_DEST);
		
		/* Filter the unimportant words */
		filterWords(DEFAULT_COUNT_DEST, DEFAULT_FILTER_WORDS_DEST);
		
		constructVectors(DEFAULT_COUNT_DEST, DEFAULT_FILTER_WORDS_DEST, DEFAULT_CONSTRUCT_VECTORS_DEST);
				
		return 0;
	}

	
	
	private int buildWordCounts(String src, String dest) throws Exception {
		
		
		Job countJob = Job.getInstance(new Configuration());
		countJob.setJarByClass(TextProcessor.class);
		
		/* Set up the inputs and mappers */
		countJob.setMapperClass(WordCountMap.class);
		countJob.setMapOutputKeyClass(Text.class);
		countJob.setMapOutputValueClass(WordQuant.class);
		
		FileInputFormat.addInputPath(countJob, new Path(src));
		countJob.setInputFormatClass(TextInputFormat.class);
		
		/* Set up the outputs and reducers */
		countJob.setReducerClass(WordCountReduce.class);
		countJob.setOutputKeyClass(Text.class);
		countJob.setOutputValueClass(Text.class);
		
		FileOutputFormat.setOutputPath(countJob, new Path(dest));
		
		
		return (countJob.waitForCompletion(true) ? 0 : 1);
	}
	
	private int filterWords(String src, String dest) throws Exception {
		Job filterJob = Job.getInstance(new Configuration());
		
		filterJob.setJarByClass(TextProcessor.class);
		
		/* Set up mapper and input */
		filterJob.setMapperClass(FilterWordsMap.class);
		filterJob.setMapOutputKeyClass(Text.class);
		filterJob.setMapOutputValueClass(WordQuant.class);
		
		filterJob.setInputFormatClass(TextInputFormat.class);
		FileInputFormat.addInputPath(filterJob, new Path(src));
		
		/* Set up reducer and output */
		filterJob.setReducerClass(FilterWordsReduce.class);
		filterJob.setOutputKeyClass(LongWritable.class);
		filterJob.setOutputValueClass(Text.class);
		
		FileOutputFormat.setOutputPath(filterJob, new Path(dest));
		
		return (filterJob.waitForCompletion(true) ? 0 : 1);
		
	}
	
	// XXX change to make use indices rather than words?
	public int constructVectors(String countSrc, String acceptableSrc, String dest) throws Exception {
		Job constructJob = Job.getInstance(new Configuration());
		
		constructJob.setJarByClass(TextProcessor.class);
		
		/* Make the white list of vocabulary words accessible from the mapper */
		constructJob.addCacheFile(new URI(acceptableSrc));
		constructJob.getConfiguration().set(getWhiteListKey(), new Path(acceptableSrc).getName());
		
		/* Set up mapper and input */
		constructJob.setMapperClass(MatrixConstructionMap.class);
		constructJob.setMapOutputKeyClass(Text.class);
		constructJob.setMapOutputValueClass(WordQuant.class);
		
		constructJob.setInputFormatClass(TextInputFormat.class);
		FileInputFormat.addInputPath(constructJob, new Path(countSrc));
		
		/* Set up reducer and output */
		constructJob.setReducerClass(WordCountReduce.class);
		constructJob.setOutputKeyClass(Text.class);
		constructJob.setOutputKeyClass(Text.class);
		
		FileOutputFormat.setOutputPath(constructJob, new Path(dest));
		
		return (constructJob.waitForCompletion(true) ? 0 : 1);
	}
	
	public static String getWhiteListKey() {
		return WHITE_LIST_KEY;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			ToolRunner.run(new Configuration(), new TextProcessor(), args);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
