#!/bin/bash
stocks=./filteredStocks.txt
output=./totalStocks.txt
while read q; do
	echo "$q" >> totalStocks.txt
done <$stocks
while read p; do
	ipo=$(echo "$p" | awk '{ print $1 }') 
	echo "$ipo" >> totalStocks.txt
done <IPOs.txt

