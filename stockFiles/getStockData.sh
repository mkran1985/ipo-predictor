#!/bin/bash
urlHead="http://finance.yahoo.com/rss/headline?s="
emptyStocks=./empty.txt
: > $emptyStocks
corpus=./corpus.txt
: > $corpus

#Iterate through all stocks in stock list
while read stock; do
	#Collect stocks from Yahoo financial
	echo $stock
	{
		wget $urlHead+$stock -O stocktemp.txt
	} &> /dev/null
	sed -e "s/<\/link>/<\/link>\\`echo -e '\n\r'`/g" < stocktemp.txt |grep -e '<link>' > newlines.txt
	sed -e 's/.*<link>\(.*\)<\/link>.*/\1/g' < newlines.txt > newlines2.txt
	sed -e "s/^http:\/\/finance[.]yahoo[.]com.*$//g" < newlines2.txt > urlParse.txt
	sed "/^$/d" urlParse.txt | sed "s/.*[*]\(.*\)$/\1/g" > url.txt
	#Clean up temp parse files
	rm stocktemp.txt
	rm newlines.txt
	rm newlines2.txt
	rm urlParse.txt
	#cat url.txt

	#Check if url list isn't empty
	if [ -s url.txt ]
	then
		while read url; do
			{
				wget $url -O newDoc.txt
			} &> /dev/null
			info=$(tr -ds '\n' ' ' < newDoc.txt | tr -d '\r' | tr -d '\r\n' | tr -d '\n\r' | tr -s '\t' ' ' | perl -0777ne 's|{.*?}||gms;print' | sed -E "s/[^ ]+[=&@#*(][^ ]*//g" | sed -E "s/[ ][=.&@#*][^ ]*//g" | sed -E 's/[.,!@#$%^&*()\[\]{}]+//g' | perl -0777ne 's|<script.*?</script>| |gms;print' | perl -0777ne 's|<.*?>| |gms;print' | tr -s " ")
			echo -e "${stock}\t${info}" >> $corpus
			echo -n "."
		done <url.txt
	else
		echo $stock >> $emptyStocks
	fi
	echo ""

	exit
done <totalStocks.txt
